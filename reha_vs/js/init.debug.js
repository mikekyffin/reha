// file:            init.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
(function ($) {
    // for IE (any). I'm *not* making cross-domain requests
    // to build our menu or anything. everything is on the same domain.
    // this shouldn't matter - but it does for IE. why? this is a hacky.
    $.support.cors = true;  
    // main init function for whole app
    reha.init = function () {
        // reference the proxy and enable it
        esri.config.defaults.io.proxyUrl = reha.config.PROXY;
        esri.config.defaults.io.alwaysUseProxy = true;
        // init necessary modules
        this.map.init();
        this.ui.init();
        this.behaviours.init();
    }
})(jQuery);