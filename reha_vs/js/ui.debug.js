// file:            ui.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
(function ($) {
    reha.ui = {
        //
        IdentifyHandler: null,

        // drawing tool bar for graphics on map - see init()
        Toolbar: null,

        //
        LayerList: null,

        // will hold the list of tabs in the right-side panel
        Tabs: null,

        // define the location search content 
        SearchContent: function (anchor) {
            var cont = $("<div>").addClass("sc-wrapper");
            $("<div>", {
                "html": "Search a location:"
            }).appendTo(cont);
            $("<input>", {
                "id": "locationSearch",
                "type": "text",
                "keyup": $.proxy(function (evt) {
                    if (evt.keyCode == "13") {
                        reha.ui.searchLocations($("#locationSearch").val(), true);
                        reha.ga.track({
                            "eventCategory": "Search",
                            "eventAction": "location",
                            "eventLabel": $("#locationSearch").val(),
                            "eventValue": null
                        });
                    }
                }, this)
            })
                .addClass("sc-input")
                .appendTo(cont);
            // a search button
            $("<button>", {
                "html": "Search",
                "id": "locSearchBtn"
            })
                .addClass("sc-button")
                .appendTo(cont)
                .button().click($.proxy(function () {
                    reha.ui.searchLocations($("#locationSearch").val(), true);
                    reha.ga.track({
                        "eventCategory": "Search",
                        "eventAction": "location",
                        "eventLabel": $("#locationSearch").val(),
                        "eventValue": null
                    });
                }, this));
            // tidy!
            $("<div>", {
                "style": "clear: both;"
            }).appendTo(cont);
            // data search
            $("<div>", {
                "html": "Search for data & maps:"
            }).appendTo(cont);
            $("<input>", {
                "id": "dataSearch",
                "type": "text",
                "keyup": $.proxy(function (evt) {
                    if (evt.keyCode == "13") {
                        reha.ui.searchServices($("#dataSearch").val(), true, "reha");
                        reha.ga.track({
                            "eventCategory": "Search",
                            "eventAction": "data",
                            "eventLabel": $("#dataSearch").val(),
                            "eventValue": null
                        });
                    }
                }, this)
            })
                .addClass("sc-input")
                .appendTo(cont);
            // button
            $("<button>", {
                "html": "Search",
                "id": "dataSearchBtn"
            })
                .addClass("sc-button")
                .button().click($.proxy(function (evt) {
                    reha.ui.searchServices($("#dataSearch").val(), true, "reha");
                    reha.ga.track({
                        "eventCategory": "Search",
                        "eventAction": "data",
                        "eventLabel": $("#dataSearch").val(),
                        "eventValue": null
                    });
                }, this))
                .appendTo(cont);
            // tidy
            $("<div>", {
                "style": "clear: both;"
            }).appendTo(cont);
            return {
                "resultElems": [],
                "domNode": cont
            }
        },

        //
        goToPlace: function (/*Object*/ place, /*Boolean*/ useProxy) {
            // make a geometry string - ugly          
            var geomStr = "{\"geometryType\":\"esriGeometryEnvelope\",\"geometries\":[{\"xmin\":" + place.minx + ",\"ymin\":" + place.miny + ",\"xmax\":" + place.maxx + ",\"ymax\":" + place.maxy + "}]}";
            // the other params
            var params = {
                "inSR": 4326,
                "outSR": 102100,
                "geometries": geomStr,
                "f": "json"
            }
            // define our call to the geometry server
            var xhr = $.getJSON(((useProxy) ? reha.config.PROXY + "?" : "") + reha.config.GEOM_SERVER + "/project" + "?" + $.param(params));
            xhr.done($.proxy(function (response) {
                if (response && response.geometries.length > 0) {
                    // just pick the first one. seriously - how many more would we need? then build an ESRI geometry extent and go there
                    var mbr = response.geometries[0];
                    var extent = new esri.geometry.Extent(
                        mbr.xmin,
                        mbr.ymin,
                        mbr.xmax,
                        mbr.ymax,
                        new esri.SpatialReference({
                            "wkid": 102100
                        }));
                    this.Map.setExtent(extent.expand(1.5));
                    var graphic = new this.extentGraphic(extent, place.name);
                    this.addExtentGraphic(graphic);
                }
            }, reha.map));
            xhr.fail(function (err) {
                // do nothing - just don't go there
                madgic.util.log(err);
            });
        },

        // create the content for the basemap panel
        BasemapContent: function () {
            // this is the info we need for the basemaps
            var _config = [{
                title: "Topographic Map",
                subtitle: "",
                name: "topo",
                url: "http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer",
                img: "images/img_topo.png"
            }, {
                title: "Satellite & Aerial Imagery",
                subtitle: "",
                name: "satellite",
                url: "http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer",
                img: "images/img_imgy.png"
            }];
            var _cont = $("<div>").addClass("bm-wrapper");
            $.each(_config, function (idx, val) {
                var map = $("<div>")
                    .addClass("bm-selector")
                    .click(function (evt) {
                        $(".bm-selector").removeClass("bm-selected");
                        map.addClass("bm-selected");
                        reha.map.Map.setBasemap(_config[idx].name);
                        reha.ga.track({
                            "eventCategory": "Basemap",
                            "eventAction": "switch",
                            "eventLabel": _config[idx].name,
                            "eventValue": null
                        });
                    })
                    .appendTo(_cont);
                if (idx == 0) {
                    map.addClass("bm-selected");
                }
                // put the thumbnail in there
                map.append($("<img>", {
                    src: _config[idx].img
                }).addClass("bm-image"));
                // put the name in there
                // we can expand this to provide a subtitle (description) if we want
                map.append($("<span>", {
                    html: _config[idx].title
                }).addClass("bm-title"));
            });
            return {
                domNode: _cont,
                setSelected: function () {
                    //stub
                },
                selected: function () {
                    //stub
                },
                isSelected: function () {
                    //stub
                }
            }
        },

        // a tab list object that defines the UI functionality
        // of the tabs themselves *and* the content divs.
        // styles in main.css.
        TabList: function (/*String*/ nodeId) {
            //TODO: use $.data() instead of [data-tab-role]
            // all the tabs
            var _tabList = $("#" + nodeId + " div");
            // iterate and define behaviours of the list
            // *and* associated div holding their content(s)
            $.each(_tabList, function (idx, node) {
                $(node).click(function () {
                    $("#" + nodeId + " div").removeClass("tab-selected");
                    $(this).addClass("tab-selected");
                    // omg i love jquery selectors. dojo is good too, just for the record. i mean that.
                    $("div[data-tab-role]").css("display", "none");
                    $("div[data-tab-role='" + this.id + "']").css("display", "block");
                });
            });
            return {
                anchor: $("#" + nodeId),
                tabs: _tabList,
                panes: $("div[data-tab-role]"),
                setSelected: function (/*String*/ nodeId) {
                    this.anchor.children().removeClass("tab-selected");
                    $("#" + nodeId).addClass("tab-selected");
                    this.panes.css("display", "none");
                    $("div[data-tab-role='" + nodeId + "']").css("display", "block");
                },
                isSelected: function (node) {
                    return $(el).hasClass("tab-selected");
                },
                destoryById: function (/*String*/ nodeId) {
                    //TODO
                }
            }
        },

        //
        searchLocations: function (/*String*/ str, /*Boolean*/ useProxy) {
            this.onSearchStart();
            var xhr = reha.search.location(/*String*/ str, /*Boolean*/ useProxy);
            xhr.done($.proxy(function (data) {
                this.onSearchEnd();
                // revrese the array so they show up in the right roder when prepended.
                data.results.places.reverse();
                $.each(data.results.places, $.proxy(function (i, obj) {
                    var view = new reha.views.LocationView(obj, this.goToPlace);
                    view.place($("#results"));
                }, this));
            }, this))
            xhr.error(function (err) {
                console.log(err);
            });
        },

        //
        searchServices: function (/*String*/ str, /*Boolean*/ useProxy, /*String*/ folder) {
            this.onSearchStart();
            var xhr = reha.search.services(str, useProxy, folder);
            xhr.done($.proxy(function (resp) {
                this.onSearchEnd();
                // revrese the array so they show up in the right order when prepended.
                resp.results.reverse();
                $.each(resp.results, $.proxy(function (i, obj) {
                    var view = new this.DataView(obj, reha.ui.addLayer);
                    view.place($("#results"));
                }, reha.views));
            }, this));
            xhr.error(function (err) {
                console.log(err);
            });
        },

        // toggle between "identify" and "download" functionality for the click event on the map.
        // do we want to find stuff or draw a rectangle - it can't be both!
        setMode: function (str) {
            switch (str) {
                case "identify":
                    this._setToIdentify();
                    break;
                case "download":
                    this._setToDownload();
                    break;
                default:
                    this._setToIdentify();
            }
        },

        //
        _setToIdentify: function () {
            this.Toolbar.deactivate();
            this.connectIdentify(reha.map.Map);
        },

        //
        _setToDownload: function () {
            this.Toolbar.activate(esri.toolbars.Draw.EXTENT);
            this.disconnectIdentify();
        },

        //
        _initTabs: function () {
            this.Tabs = new this.TabList("tabs");
            this.Tabs.setSelected("layersTab");
        },

        //
        _initLinks: function () {
            var mp = new reha.widgets.MenuPanel("links", $("#links"));
            var linkList = $("<div>")
                .addClass("lk-wrapper");
            $.each(reha.config.LINKS, function (i, o) {
                var select = $("<div>")
                    .addClass("lk-selector")
                    .click(function () {
                        reha.ga.track({
                            "eventCategory": "Link",
                            "eventAction": "click",
                            "eventLabel": o["href"],
                            "eventValue": null
                        })
                    })
                    .appendTo(linkList);
                var link = $("<a>", {
                    "href": o["href"],
                    "target": "_blank"
                })
                    .append($("<img>", {
                        "src": o["img"]
                    }).addClass("lk-image"))
                    .append($("<span>", {
                        "html": o["label"]
                    }).addClass("lk-label"))
                    .appendTo(select);
            });
            mp.place();
            mp.setContent($("<div>", {
                html: linkList
            }));
        },

        // TODO: needs error handling - what if services are down?
        _initMenu: function () {

            var dataMenu = new reha.widgets.MenuPanel("data", $("#data"));
            dataMenu.place();
            dataMenu.startLoading();
            var menu = new reha.widgets.Menu(reha.config.PROXY + "?" + reha.config.MENU, false, null);
            var get = menu.request("reha");
            get.done($.proxy(function (data) {
                // filter the data for non-topo stuff
                var items = $.map(data.menu, function (coll, i) {
                    return coll.name.match(/topos - /i) ? null : coll;
                });
                var ui = menu.render(items, dataMenu.anchor, this.addLayer);
                dataMenu.setContent(ui);
                dataMenu.stopLoading();
            }, this));

            var topoMenu = new reha.widgets.MenuPanel("topo", $("#topo"));
            topoMenu.place();
            topoMenu.startLoading();

            var menu = new reha.widgets.Menu(reha.config.PROXY + "?" + reha.config.MENU, false, null);
            var get = menu.request("reha");
            get.done($.proxy(function (data) {
                // filter the data for topo stuff
                var items = $.map(data.menu, function (coll, i) {
                    return coll.name.match(/topos - /i) ? coll : null;
                });
                // TODO - maybe the buse of the anchor to render and internal thing. -WHAT!? I have no idea what I was talking about.
                var ui = menu.render(items, topoMenu.anchor, this.addLayer);
                topoMenu.setContent(ui);
                topoMenu.stopLoading();
            }, this));
            //
            var apMenu = new reha.widgets.MenuPanel("airphoto", $("#airphoto"));
            apMenu.place();
            apMenu.startLoading();
            var menu = new reha.widgets.Menu(null, false, null);
            var obj = [
                {
                    "items": [],
                    "name": $("<span>", {
                        "html": "National Air Photo Library Metadata Viewer"
                    }).click(function () {
                        window.open("http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? document.location.host : "madgic.trentu.ca") + "/napl", "_blank");
                    })
                }, {
                    "items": [],
                    "name": $("<span>", {
                        "html": "View and download air photos"
                    }).click(function () {
                        window.open("http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? document.location.host : "madgic.trentu.ca") + "/airphoto", "_blank");
                    })
                }
            ];
            var ui = menu.render(obj, apMenu.anchor, null);
            apMenu.setContent(ui);
            apMenu.stopLoading();
        },

        // 
        _viewEvents: {
            "toggle": function (/*String*/ display, /*String*/ icon, /*String*/ type, /*String*/ title) {
                if ($(display).css("display") == "none") {
                    $(display).css("display", "block");
                    $(icon).removeClass("ui-icon-triangle-1-e");
                    $(icon).addClass("ui-icon-triangle-1-s");
                    reha.ga.track({
                        "eventCategory": "Layer",
                        "eventAction": type,
                        "eventLabel": title,
                        "eventValue": null
                    });
                } else {
                    $(display).css("display", "none");
                    $(icon).removeClass("ui-icon-triangle-1-s");
                    $(icon).addClass("ui-icon-triangle-1-e");
                }
            },
            "zoomToLayer": function (layer) {
                reha.map.zoomToLayer(layer);
                reha.ga.track({
                    "eventCategory": "Layer",
                    "eventAction": "zoom to",
                    "eventLabel": layer["id"],
                    "eventValue": null
                });
            },
            "removeLayer": function (layer, name) {
                reha.ui.removeLayer(layer, name);
            },
            "onOpacityChange": function (props, elem) {
                if ($.notify) {
                    $.notify.note(props.title + ": is now " + (100 - parseInt(elem.slider("value"))) + "% transparent", "info", 3);
                }
            },
            "openSymbologyDialog": function (layer, view) {
                var fieldTask = new reha.renderers.RendererFieldTask(layer);
                reha.ui.setDialogTitle("Symbolize");
                reha.ui.setDialogContent($("<div>", {
                    html: "<i class=\"fa fa-cog fa-spin\"></i>" + "...querying data fields"
                }));
                reha.ui.openDialog();
                $(fieldTask).on("get-fields-complete", function () {
                    var sym = new reha.ui.SymbologyUI(layer, fieldTask.fields, view);
                    reha.ui.setDialogContent(sym.domNode);
                });
            }
        },
        SymbologyUI: function (layer, fields, view) {

            this.domNode = $("<div>", {
                "class": "sym-ui"
            });

            var unique = $("<div>", {
                html: $("<div>Unique Values</div>")
            }).appendTo(this.domNode);

            var classBreak = $("<div>", {
                html: $("<div>Class Breaks</div>")
            }).appendTo(this.domNode);

            var uniqueSelect = $("<select>", {
                "class": "sym-ui-select-field"
            }).appendTo(unique);

            $("<div>", { "style": "clear:both" }).appendTo(unique);

            var uniqueApplyButton = $("<div>", {
                "class": "sym-ui-button",
                "html": $("<span>", {
                    "html": "<i class=\"fa fa-check-circle\"></i>&nbsp;Apply"
                }),
                "click": function () {
                    var renderer = new reha.renderers.UniqueValuesRenderer(layer, uniqueSelect.val(), $(uniqueSelect).find(":selected").data("layer-index"));
                    renderer.renderRandom(function () {
                        layer.applyRenderer(parseInt($(classSelect).find(":selected").data("layer-index")), renderer);
                        view.setLegendFromRenderer(renderer);
                    });
                }
            }).appendTo(unique);

            var uniqueRevertButton = $("<div>", {
                "class": "sym-ui-button",
                "html": $("<span>", {
                    html: "<i class=\"fa fa-circle\"></i>&nbsp;Revert"
                }),
                "click": function () {
                    layer.removeRenderer();
                    view.setLegendFromService();
                }
            }).appendTo(unique);

            var classSelect = $("<select>", {
                "class": "sym-ui-select-field"
            }).appendTo(classBreak);

            var classMethodSelect = $("<select>", {
                "class": ""
            }).appendTo(classBreak);

            var classMethods = [
                {
                    value: "natural-breaks",
                    name: "Natural Breaks"
                },
                {
                    value: "equal-interval",
                    name: "Equal Interval"
                },
                {
                    value: "quantile",
                    name: "Quantile"
                },
                {
                    value: "standard-deviation",
                    name: "Standard Deviation"
                },
                {
                    value: "geometrical-interval",
                    name: "Geometrical Interval"
                }
            ];
            classMethods.forEach(function (item) {
                classMethodSelect.append($("<option>", {
                    "html": item.name,
                    "value": item.value
                }));
            });

            var numOfClassesSelect = $("<select>").appendTo(classBreak);
            for (var n = 3; n < 8; n++) {
                $("<option>", {
                    html: n,
                    value: n
                }).appendTo(numOfClassesSelect);
            }

            $("<div>", { "style": "clear:both" }).appendTo(classBreak);

            var colorList = $("<ul>", {
                "class": "sym-ui-color-list"
            }).appendTo(classBreak);

            var classColors = [
                {
                    "from": "#FEE0D2",
                    "to": "#DE2D26"
                },
                {
                    "from": "#E5F5E0",
                    "to": "#31A354"
                },
                {
                    "from": "#F0F0F0",
                     "to": "#3182BD"
                },
                {
                    "from": "#DEEBF7",
                    "to": "#636363"
                },
                {
                    "from": "#EFEDF5",
                    "to": "#756BB1"
                }
            ];
            $.each(classColors, function (i, item) {
                $("<li>", {
                    "class": "sym-ui-color-item",
                    "data-from": item.from,
                    "data-to": item.to,
                    "data-current": (i === 0) ? "is-current" : "",
                    "click": function () {
                        $.each($(".sym-ui-color-item", classBreak), function (i, elem) {
                            $(elem).attr("data-current", "");
                        });                      
                        $(this).attr("data-current", "is-current");
                    }
                })
                    //<i class="fa fa-arrows-h"></i>
                    .appendTo(colorList)
                    .append($("<div>", {
                        "class": "sym-ui-from-hex",
                        "style": "background-color:" + item.from
                    }))
                    .append($("<div>", {
                        "html": "<i class=\"fa fa-arrows-h\"></i>",
                        "class": "sym-ui-hex-spacer",
                        "style": "float:left;margin: 0 px 3px;"
                    }))
                    .append($("<div>", {
                        "class": "sym-ui-to-hex",
                        "style": "background-color:" + item.to
                    }));
            });

            $("<div>", { "style": "clear:both" }).appendTo(classBreak);

            var classApplyButton = $("<div>", {
                "class": "sym-ui-button",
                "html": $("<span>", {
                    "html": "<i class=\"fa fa-check-circle\"></i>&nbsp;Apply"
                }),
                "click": function () {
                    var renderer = new reha.renderers.ClassBreaksRenderer(layer, $(classSelect).find(":selected").data("layer-index"), classSelect.val(), $(classMethodSelect).val());
                    var colorSet = colorList.find("[data-current=is-current]");
                    renderer.renderColorScheme(
                        numOfClassesSelect.val(),
                        colorSet.data("from"),
                        colorSet.data("to"),
                        function (renderer) {
                            layer.applyRenderer(parseInt($(classSelect).find(":selected").data("layer-index")), renderer);
                            view.setLegendFromRenderer(renderer);
                        }
                    );
                }
            }).appendTo(classBreak);

            var classRevertButton = $("<div>", {
                "class": "sym-ui-button",
                "html": $("<span>", {
                    html: "<i class=\"fa fa-circle\"></i>&nbsp;Revert"
                }),
                "click": function () {
                    layer.removeRenderer();
                    view.setLegendFromService();      
                }
            }).appendTo(classBreak);

            // IMPORTANT - fill fields to appropriate drop down
            $.each(fields, function (i, field) {
                var option = $("<option>", {
                    "html": field.name,
                    "value": field.name,
                    "data-field-type": field.type,
                    "data-layer-index": field.layerIndex
                });
                if (field.type.match(/^esriFieldType(double|single|smallinteger|integer)$/ig)) {
                    option.appendTo(classSelect);
                } else if ((field.type.match(/^esriFieldTypeString$/ig))) {
                    option.appendTo(uniqueSelect);
                }
            });

            return this;
        },
        //
        _initSearch: function () {
            var mp = new reha.widgets.MenuPanel("search", $("#searchSelector"));
            mp.place();
            mp.setContent($("<div>", {
                html: new this.SearchContent().domNode
            }));
        },

        // create a menu panel and place the basemap switching contents in it
        _initBasemapSwitch: function () {
            var mp = new reha.widgets.MenuPanel("search", $("#basemapSelector"));
            mp.place();
            mp.setContent($("<div>", {
                html: new this.BasemapContent().domNode
            }));
        },

        //
        _initToolbar: function () {
            this.Toolbar = new esri.toolbars.Draw(reha.map.Map);
            this.Toolbar.fillSymbol = new esri.symbol.SimpleFillSymbol(
                esri.symbol.SimpleFillSymbol.STYLE_SOLID,
                new esri.symbol.SimpleLineSymbol(
                    esri.symbol.SimpleLineSymbol.STYLE_SOLID,
                    new dojo.Color([0, 0, 0]), 2), //black
                    new dojo.Color([255, 255, 0, 0.25]) //yellow
            );
            dojo.connect(this.Toolbar, "onDrawEnd", this, function (ext) {
                // clear previous stuff
                reha.map.Map.graphics.clear();
                $(".svgBtn").remove();
                var graphic = new esri.Graphic(ext, this.Toolbar.fillSymbol);
                $.extend(graphic, {
                    "isDownloadGraphic": true
                });
                reha.map.Map.graphics.add(graphic);
                var svg = this._getSVG(graphic);
                var buttons = new reha.widgets.DownloadButtons(
                    svg,
                    graphic,
                    $.proxy(function () {
                        this._downloadAll(graphic);
                    }, this),
                    this._clearDownloadExtent);
                buttons.place($("#map"));
            });
        },

        //
        _getSVG: function (graphic) {
            return (graphic.getDojoShape()) ? graphic.getDojoShape().getNode() : null;
        },

        // remove the downoad graphic(s) and fing any of those download buttons and remove them too.
        _clearDownloadExtent: function () {
            reha.map.Map.graphics.clear();
            $(".svgBtn").remove();
        },

        //
        _logDownload: function (id, extent) {
            reha.ga.track({
                "eventCategory": "Download",
                "eventAction": id,
                "eventLabel": JSON.stringify({
                    "xmin": extent.xmin,
                    "ymin": extent.ymin,
                    "xmax": extent.xmax,
                    "ymax": extent.ymax,
                }),
                "eventValue": null
            });
        },

        // look in the list of layers and get them all! muhahahaha!
        _downloadAll: function (graphic) {
            // just for logging
            var geographic = esri.geometry.webMercatorToGeographic(new esri.geometry.Extent({
                "xmin": graphic.geometry.xmin,
                "ymin": graphic.geometry.ymin,
                "xmax": graphic.geometry.xmax,
                "ymax": graphic.geometry.ymax,
                "spatialReference": { "wkid": 102100 }
            }));
            $.each(reha.layers.Layers, $.proxy(function (i, val) {
                // track with ga
                this._logDownload(val["title"], geographic);
                // add extent to object sent to setup download
                $.extend(val, {
                    "xmin": graphic.geometry.xmin,
                    "ymin": graphic.geometry.ymin,
                    "xmax": graphic.geometry.xmax,
                    "ymax": graphic.geometry.ymax
                });
                if (val.type === "Image Service") {
                    reha.util.log("...initiating raster download.");
                    var dl = new reha.download.RasterDownload(val);
                    dl.submitJob();
                } else if (val.type === "Map Service") {
                    reha.util.log("...initiating vector download.");
                    var dl = new reha.download.VectorDownload(val);
                    dl.submitJob();
                } else if (val.type === "Air Photo Service") {
                    reha.util.log("...initiating air photo download.");
                    var dl = new reha.download.AirPhotoDownload(val);
                    dl.submitJob();
                } else {
                    reha.util.log("Error: unable to download '" + val.name + "' from " + val.url + ". Not an image or map service.");
                }
            }, this));
        },

        //
        onSearchStart: function () {
            this.clearSearch();
            reha.ui.Tabs.setSelected("resultsTab");
            $("#searchLoader").css("display", "block");
        },

        //
        onSearchEnd: function () {
            $("#searchLoader").css("display", "none");
        },

        // empty all the search results
        clearSearch: function () {
            $("#results").empty();
        },

        // empty all the search results
        clearDownload: function () {
            $("#downloadList").empty();
        },

        //
        _initDialog: function (node) {
            node.dialog();
            node.dialog("option", "width", 375);
            node.dialog("option", "height", 400);
            $(".ui-dialog button").prop("title", "");
            this.closeDialog();
        },

        //
        setDialogFromAsyncHtml: function (page, title) {
            this.setDialogTitle(title);
            this.setDialogContent($("<img>", {
                src: "images/gear_5F89AA_FEFEFE_64x64.gif",
                style: "display: block; margin-left: auto; margin-right: auto;"
            }));
            $("#dialog").load(page);
        },

        setDialogSize: function (height, width) {
            $("#dialog").dialog("option", "height", height);
            $("#dialog").dialog("option", "width", width);
        },
        //
        setDialogContent: function (/*String or DomNode*/ content) {
            $("#dialog").html(content);
        },

        //
        setDialogTitle: function (/*String*/ text) {
            $("#dialog").dialog("option", "title", text);
        },

        //
        openDialog: function () {
            $("#dialog").dialog("open");
        },

        //
        closeDialog: function () {
            $("#dialog").dialog("close");
        },

        //
        removeLayer: function (layer, name) {
            reha.map.removeLayer(layer);
            reha.layers.removeLayer(name);
        },

        //
        addLayer: function (obj, isAirphoto) {
            // check to see if it's on the map already
            if (!reha.map.hasLayer(obj.title)) {
                // check to see if it's an air photo layer and assign Layer and View appropriately
                var layer = new reha.layers.Layer(obj, (isAirphoto) ? true : false, (isAirphoto) ? "airphoto" : null);
                var view = (!isAirphoto) ? new reha.views.LayerView(layer, obj, reha.ui._viewEvents) : new reha.views.AirphotoView(layer, obj, reha.ui._viewEvents);
                var model = new reha.models.LayerModel(layer, view, obj);
                view.place(reha.ui.LayerList);
                reha.layers.Layers.push(model);
                reha.map.Map.addLayer(layer);
                reha.ga.track({
                    "eventCategory": "Layer",
                    "eventAction": "add",
                    "eventLabel": obj["title"],
                    "eventValue": null
                });
            }
        },

        // great - we've found some results
        onSearchAirphotoSuccess: function (data) {
            var container = $("<div>", {
                "class": "content"
            }).appendTo($(".actionsPane .identify-air-photo-content"));
            var msg = $("<div>", {
                "html": "<i class=\"fa fa-plus\"></i>Click to add to map: ",
                "class": "success-message"
            }).appendTo(container);
            $.each(data.results, function (i, specs) {
                var year = $("<span>", {
                    "html": specs.title.substring(1),
                    "click": function () {
                        this.addLayer(specs, true);
                    }.bind(this)
                }).appendTo(container);
            }.bind(this));
            container.fadeIn();
        },

        // no results have been found - queries returned empty array as result (no error)
        onSearchAirphotoNoResult: function (msg) {
            var container = $("<div>", {
                "class": "content"
            }).appendTo($(".actionsPane .identify-air-photo-content"));
            var msg = $("<div>", {
                "html": "<i class=\"fa fa-question-circle\"></i>" + ((msg) ? msg : ""),
                "class": "warning-message"
            }).appendTo(container);
            container.fadeIn();
        },

        // empty ".identify-air-photo-content" when a search starts
        onSearchAirphotoStart: function () {
            $(".actionsPane .identify-air-photo-content").empty();
        },

        // remove the spinning cog whenever the airphoto search ends
        onSearchAirphotoEnd: function () {
            $(".actionsPane .identify-air-photo-content .progress").remove();
        },

        // if an error is thrown, show an icon and message
        onSearchAirphotoError: function (msg) {
            var container = $("<div>", {
                "class": "content"
            }).appendTo($(".actionsPane .identify-air-photo-content"));
            var msg = $("<div>", {
                "html": "<i class=\"fa fa-exclamation-circle\"></i>" + ((msg) ? msg : ""),
                "class": "error-message"
            }).appendTo(container);
            container.fadeIn();
        },

        // display a spinning cog icon and msg
        onSearchAirphotoInProgress: function (msg) {
            var container = $("<div>", {
                "style": "text-align:center;margin: 3px 0 0 0;",
                "class": "progress content"
            }).appendTo($(".actionsPane .identify-air-photo-content"));
            var msg = $("<div>", {
                "html": "<i class=\"fa fa-cog fa-spin\"></i>" + ((msg) ? msg : ""),
                "class": "progress-message"
            }).appendTo(container);
            container.fadeIn();
        },

        // convert 'arguments' - which holds the results from all the async requests - into something more useful
        // for display
        _resultsToMatches: function (resultArguments) {
            var matches = { "yearsMatching": 0, "photosMatching": 0, "results": [] }
            var results = $.each(resultArguments, function (i, year) {
                if (year[0].results.length > 0) { // if it doesn't have any results just ignore it
                    matches.results.push({
                        url: reha.config.SERVICE_BASE.replace(/reha/ig, "airphoto") + "/" + year[0].results[0].layerName + "/MapServer",
                        name: year[0].results[0].layerName,
                        title: year[0].results[0].layerName,
                        year: parseInt(year[0].results[0].layerName.substring(1)),
                        photos: year[0].results
                    });
                    matches.yearsMatching++;
                    matches.photosMatching = matches.photosMatching + year[0].results.length;
                    matches.results.sort(function (a, b) { return (a.year > b.year) ? 1 : ((b.year > a.year) ? -1 : 0); });
                }
            });
            return matches;
        },

        // create a list of async tasks from reha.config.AIRPHOTO_SERVICES - which is loaded at the beginning of the
        // session. Run an identify operation on each and look for air photos at event point
        searchAirphotos: function (evt) {
            this.onSearchAirphotoStart();
            this.onSearchAirphotoInProgress("Searching air photos");
            var xhr = null;
            if (reha.config.AIRPHOTO_SERVICES) {
                // determine all the services that need to be queried
                var tasks = $.map(reha.config.AIRPHOTO_SERVICES, function (item, i) {
                    return $.getJSON(reha.config.PROXY + "?" + item.url + "/identify" + "?" + $.param({
                        "f": "json",
                        "geometryType": "esriGeometryPoint",
                        "geometry": evt.data.point.x + "," + evt.data.point.y,
                        "layers": 0,
                        "tolerance": 2,
                        "mapExtent": evt.data.map.extent.xmin + "," + evt.data.map.extent.ymin + "," + evt.data.map.extent.xmax + "," + evt.data.map.extent.ymax,
                        "sr": evt.data.map.spatialReference.wkid,
                        "imageDisplay": window.screen.height + "," + window.screen.width + "," + "96",
                        "returnGeometry": true
                    }));
                });
                // search!
                xhr = reha.search.airphotos(this, tasks);
                // don't bother checking the response, just look at the 'arguments' to see
                // all the results - some will propbaly be empty.
                xhr.done(function () {
                    var matches = this._resultsToMatches(arguments);

                    if (matches.yearsMatching > 0) {
                        this.onSearchAirphotoSuccess(matches);
                    } else {
                        this.onSearchAirphotoNoResult("No air photos at this location");
                    }
                }.bind(this));
                // this doesn't include errors that occurs on a single request in the list of requests
                xhr.fail(function (err) {
                    // just kidding - we'll need a better message here
                    this.onSearchAirphotoError("Error 99999: unspecified error.");
                }.bind(this));
                // 
                xhr.always(function () {
                    this.onSearchAirphotoEnd();
                }.bind(this));
            }
        },

        //
        connectIdentify: function (map) {
            // set click handler
            this.IdentifyHandler = dojo.connect(map, "onClick", this, function (evt) {
                this._removeAirphotosForIdentify();
                $(".identify-air-photo-link", map.infoWindow.domNode).off("click");
                // regular feature identify stuff - will also handle pixel values from image services.
                reha.identify.identify(map, evt.mapPoint.x, evt.mapPoint.y, new reha.identify.IdentifyList(map));
                var num = map.layerIds.length - 1;
                map.infoWindow.setContent("Searching (" + num + ") layers for features!");
                map.infoWindow.show(evt.mapPoint);
                // bind callback to the click event of the "airphoto search link"
                $(".identify-air-photo-link", map.infoWindow.domNode).click({
                    "map": map,
                    "point": evt.mapPoint
                }, function (evt) {
                    (reha.config.AIRPHOTO_SERVICES) ? this.searchAirphotos(evt) : this.onSearchAirphotoError("Could not load airphotos");
                }.bind(this));
            });
        },

        // disconnect the handler from the map -  generally so we can connect the download handler
        disconnectIdentify: function () {
            dojo.disconnect(this.IdendtifyHandler);
        },

        //
        _removeAirphotosForIdentify: function () {
            $(".actionsPane .identify-air-photo-content").empty();
            for (var i = reha.layers.Layers.length - 1; i >= 0; i--) {
                if (reha.layers.Layers[i].layer.isAirphoto) {
                    reha.layers.Layers[i].view.destroy();
                    reha.ui.removeLayer(reha.layers.Layers[i].layer, reha.layers.Layers[i].layer.name);
                }
            }
        },

        //
        _initIdentify: function (map) {
            this.setMode("identify");
            // UI additions
            $("<a>", {
                "href": "#",
                "html": "Search air photos here",
                "class": "identify-air-photo-link"
            }).appendTo($(".actionsPane .actionList", map.infoWindow.domNode));

            $("<div>", {
                "html": "",
                "class": "identify-air-photo-content"
            }).appendTo($(".actionsPane .actionList", map.infoWindow.domNode));

            $("<i>", {
                "class": "fa fa-map-marker"
            }).prependTo($(".actionsPane .actionList .zoomTo", map.infoWindow.domNode));

            $("<i>", {
                "class": "fa fa-search"
            }).prependTo($(".actionsPane .actionList .identify-air-photo-link", map.infoWindow.domNode));
        },

        // initialize the UI!
        init: function () {
            // make sure the list of layers is a sortable container
            // TODO - move this to reha.behaviours?
            this.LayerList = $("#layers").sortable({
                stop: function (event, ui) {
                    reha.map.reorderLayers();
                }
            });
            this._initTabs();
            this._initMenu();
            this._initSearch();
            this._initBasemapSwitch();
            this._initLinks();
            this._initDialog($("#dialog"));
            this._initToolbar();
            this._initIdentify(reha.map.Map);
        }
    }
})(jQuery);