﻿// file:            util.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
(function ($) {
    reha.util = {
        // log to console
        log: function (txt) {
            if (reha.config.IS_DEBUG) {
                console.log(txt);
            }
        }
    }
})(window.jQuery);