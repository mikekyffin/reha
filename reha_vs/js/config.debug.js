// file:            config.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
(function ($) {
    reha.config = {
        // the roor of all services - i.e. arcgis server
        SERVICE_BASE: "http://" + ((document.location.host.match(/^albers|madgic3|204.225.10.89|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/reha",
        //
        IS_DEBUG: (document.location.host.match(/^albers|madgic3|localhost/i)) ? true : false,
        //
        APPLICATION_NAME: "reha",
        // resource path for menu info
        MENU: "http://" + ((document.location.host.match(/^albers|madgic3|204.225.10.89|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/resources/services/getServiceDefinitions.ashx",
        // resource path for menu info
        SEARCH_URL: "http://" + ((document.location.host.match(/^albers|madgic3|204.225.10.89|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/resources/services/searchServiceDefinitions.ashx",
        // path to proxy
        PROXY: "handlers/proxy.ashx",
        // url for location search via NRCan
        GEO_SEARCH: "http://geogratis.gc.ca/loc/en/loc",
        //
        GEOM_SERVER: "http://" + ((document.location.host.match(/^albers|madgic3|204.225.10.89|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/Utilities/Geometry/GeometryServer",
        // clip & download vector 
        DOWNLOAD_VECTOR_URL: "http://" + ((document.location.host.match(/^albers|madgic3|204.225.10.89|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/tools/ClipFromWeb/GPServer/ClipFromWeb",
        // select & download raster
        DOWNLOAD_RASTER_URL: "http://" + ((document.location.host.match(/^albers|madgic3|204.225.10.89|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/tools/GetMap/GPServer/GetMap",
        DOWNLOAD_AIR_PHOTO_URL: "http://" + ((document.location.host.match(/^albers|madgic3|204.225.10.89|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/tools/GetTile/GPServer/GetTile",
        // download authenitcation for zip file
        DOWNLOAD_AUTH: "http://web2.trentu.ca:2048/login?url=",
        // download license disclaimer, etc.
        DOWNLOAD_DISCLAIMER: "http://lonestar.trentu.ca/ORTHOS/login/ap_disclaimer.asp",
        // links - for the university links panel
        LINKS: [
            {
                "img": "images/img_trent.png",
                "label": "Trent University",
                "href": "http://www.trentu.ca/"
            },
            {
                "img": "images/img_bata.png",
                "label": "Bata Library",
                "href": "http://www.trentu.ca/library/"
            },
            {
                "img": "images/img_map.png",
                "label": "MaDGIC",
                "href": "http://www.trentu.ca/library/madgic/index.htm"
            },
            {
                "img": "images/img_reha.png",
                "label": "REHA",
                "href": "http://trentu.ca/library/madgic/reha.htm"
            }
        ],
        MAP_LODS: [
           {
               "level": 0,
               "resolution": 156543.03392800014,
               "scale": 5.91657527591555E8
           },
           {
               "level": 1,
               "resolution": 78271.51696399994,
               "scale": 2.95828763795777E8
           },
           {
               "level": 2,
               "resolution": 39135.75848200009,
               "scale": 1.47914381897889E8
           },
           {
               "level": 3,
               "resolution": 19567.87924099992,
               "scale": 7.3957190948944E7
           },
           {
               "level": 4,
               "resolution": 9783.93962049996,
               "scale": 3.6978595474472E7
           },
           {
               "level": 5,
               "resolution": 4891.96981024998,
               "scale": 1.8489297737236E7
           },
           {
               "level": 6,
               "resolution": 2445.98490512499,
               "scale": 9244648.868618
           },
           {
               "level": 7,
               "resolution": 1222.992452562495,
               "scale": 4622324.434309
           },
           {
               "level": 8,
               "resolution": 611.4962262813797,
               "scale": 2311162.217155
           },
           {
               "level": 9,
               "resolution": 305.74811314055756,
               "scale": 1155581.108577
           },
           {
               "level": 10,
               "resolution": 152.87405657041106,
               "scale": 577790.554289
           },
           {
               "level": 11,
               "resolution": 76.43702828507324,
               "scale": 288895.277144
           },
           {
               "level": 12,
               "resolution": 38.21851414253662,
               "scale": 144447.638572
           },
           {
               "level": 13,
               "resolution": 19.10925707126831,
               "scale": 72223.819286
           },
           {
               "level": 14,
               "resolution": 9.554628535634155,
               "scale": 36111.909643
           },
           {
               "level": 15,
               "resolution": 4.77731426794937,
               "scale": 18055.954822
           },
           {
               "level": 16,
               "resolution": 2.388657133974685,
               "scale": 9027.977411
           },
           {
               "level": 17,
               "resolution": 1.1943285668550503,
               "scale": 4513.988705
           },
           {
               "level": 18,
               "resolution": 0.5971642835598172,
               "scale": 2256.994353
           }
           //,{
           //    "level": 19,
           //    "resolution": 0.29858214164761665,
           //    "scale": 1128.497176
           //}
        ],

    }
    var assetUrl = reha.config.PROXY + "?" + reha.config.MENU + "?" + $.param({ dir: "airphoto", f: "json" });
    var xhr = $.getJSON(assetUrl);
    xhr.done(function (resp) {
        reha.config.AIRPHOTO_SERVICES = resp.menu[0].items;
    }.bind(this));
    xhr.fail(function (err) {
        reha.config.AIRPHOTO_SERVICES = null;
    });
})(jQuery);