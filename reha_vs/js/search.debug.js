// file:            search.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

/**
* reha MBR:
*   left: -78.902968
*   top: 45.490726
*   right: -77.398487
*   bottom: 43.869983
*/

var reha = reha || {};
(function ($) {
    reha.search = {
        // return results from keyword matching. actually requires keywords to be entered.      
        services: function (/*String*/ str, /*Boolean*/ useProxy, /*String*/ folder) {
            return $.getJSON(((useProxy) ? reha.config.PROXY + "?" : "") + reha.config.SEARCH_URL + "?" + $.param({
                "dir": folder,
                "q": str
            }));
        },
        // returns results from a geographic placename search @ nrcan
        location: function (/*String*/ str, /*Boolean*/ useProxy) {
            var params = {
                "q": str,
                "bbox": "-78.902968,43.869983,-77.398487,45.490726"
            }
            return $.getJSON(((useProxy) ? reha.config.PROXY + "?" : "") + reha.config.GEO_SEARCH + "?" + $.param({
                "q": str,
                "bbox": "-78.902968,43.869983,-77.398487,45.490726"
            }));
        },
        // check all the pre-fetched air photo services
        airphotos: function (scope, tasks) {
            return $.when.apply(scope, tasks);
        }
    }
})(window.jQuery);