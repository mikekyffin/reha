// file:            map.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
(function ($) {
    reha.map = {
        // reference to the map
        Map: null,
        // create the map
        init: function () {
            
            var popup = new esri.dijit.Popup(null, dojo.create("div"));
            this.Map = new esri.Map("map", {
                infoWindow: popup,
                basemap: "topo",
                center: [-78.290319, 44.718075], //long, lat
                zoom: 9,
                sliderStyle: "small",
                logo: true,
                lods: reha.config.MAP_LODS,
                showAttribution: false
            });
            dojo.connect(this.Map, "onLoad", this, function (_map) {

                // add this to force Dojo to add a random variable to GP GET URLs. important for IE11.
                // http://forums.arcgis.com/threads/88625-IE-caching-responses-to-GP.submitJob-callbacks?highlight=submitjob
                esri.setRequestPreCallback(function (ioArgs) {
                    if (ioArgs.url.indexOf("GPServer") > -1) {
                        ioArgs.preventCache = true;
                    }
                    return ioArgs;
                });

                dojo.connect(this.Map, "onUpdateStart", this, function () {
                    // start showing the updating spinner
                    $("#spinner").css("display", "block");
                    $(".svgBtn").remove();
                });

                dojo.connect(this.Map, "onZoomStart", this, function () {
                    $(".svgBtn").remove();
                });

                dojo.connect(this.Map, "onUpdateEnd", reha.map, function () {
                    // stop showing the updating spinner
                    $("#spinner").css("display", "none");

                    var dlGraphics = $.map(_map.graphics.graphics, function (item) {
                        return (item.isDownloadGraphic && item.isDownloadGraphic === true) ? item : null;
                    });

                    $.each(dlGraphics, $.proxy(function (i, graphic) {
                        var svg = this._getSVG(graphic);
                        var buttons = new reha.widgets.DownloadButtons(
                            svg,
                            graphic,
                            $.proxy(function () {
                                this._downloadAll(graphic);
                            }, this),
                            this._clearDownloadExtent
                        );
                        buttons.place($("#map"));
                    }, reha.ui));

                    var loGraphics = $.map(_map.graphics.graphics, function (item) {
                        return (item.isLocationGraphic && item.isLocationGraphic === true) ? item : null;
                    });

                    $.each(loGraphics, $.proxy(function (i, graphic) {
                        if (graphic.visible && graphic.getDojoShape(graphic)) {
                            this.fadeExtentGraphic(graphic);
                        }
                    }, this));                   
                });
            });
        },
        // add a layer to the map
        addLayer: function (/*esri.Layer*/ layer) {
            this.Map.addLayer(layer);
        },
        //
        extentGraphic: function(extent, title) {
            var graphic = new esri.Graphic(extent, new esri.symbol.SimpleFillSymbol(
                esri.symbol.SimpleFillSymbol.STYLE_SOLID,
                new esri.symbol.SimpleLineSymbol(
                    esri.symbol.SimpleLineSymbol.STYLE_SOLID,
                    new dojo.Color([39, 121, 170]), 2),
                new dojo.Color([255, 255, 0, 0.25])
            ));
            $.extend(graphic, {
                "isLocationGraphic": true,
                "title": title
            });           
            return graphic;
        },
        //
        addExtentGraphic: function(graphic){
            this.Map.graphics.add(graphic);
        },
        //
        fadeExtentGraphic: function(graphic) {
            var svg = graphic.getDojoShape(graphic).getNode();
            var i = $(svg).css("opacity");
            var fade = window.setInterval($.proxy(function () {
                if (i > 0) {
                    i = i - 0.001;
                    $(svg).css("opacity", i);
                } else {
                    clearInterval(fade);
                    this.Map.graphics.remove(graphic);
                }
            }, this), 1);
        },
        // remove a layer from the map
        removeLayer: function (/*esri.Layer*/ layer) {
            this.Map.removeLayer(layer);
        },
        // check to see if the layer is already on the map using the layers ID. note: needs ID.
        hasLayer: function(layerId) {
            return $.inArray(layerId, this.Map.layerIds) >= 0;
        },
        // zoom to the extent of a layer
        zoomToLayer: function (/*esri.Layer*/ layer) {
            this.Map.setExtent(layer.fullExtent);
        },
        // reorder the layers based on the list of layer <li> UI elements - collection of layer views
        reorderLayers: function() {
            var k, lyr;
            var lyrList = $.map($("#layers li"), function (val, idx) {
                return $(val).attr("layer");
            });
            for (k = lyrList.length - 1; k > -1; k--) {
                this.Map.reorderLayer(this.Map.getLayer(lyrList[k]), this.Map.layerIds.length - 1);
            }
        }
    }
})(jQuery);