// file:            layers.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
(function ($) {
    
    // _DynamicLayer: extend ArcGISDynamicMapServiceLayer to include a function to assign a renderer to
    // itself.
    function _DynamicLayer(_url, _params) {
        esri.layers.ArcGISDynamicMapServiceLayer.call(this, _url, _params);
    }
    _DynamicLayer.prototype = Object.create(esri.layers.ArcGISDynamicMapServiceLayer.prototype);
    _DynamicLayer.prototype.applyRenderer = function (_idx, _renderer) {
        var layerDrawingOptions = [];
        var layerDrawingOption = new esri.layers.LayerDrawingOptions();
        layerDrawingOption.renderer = _renderer;
        layerDrawingOptions[_idx] = layerDrawingOption;
        this.setLayerDrawingOptions(layerDrawingOptions);
    }
    _DynamicLayer.prototype.removeRenderer = function () {
        this.setLayerDrawingOptions([]);
    }
    // _ImageLayer:
    function _ImageLayer(_url, _params) {
        esri.layers.ArcGISImageServiceLayer.call(this, _url, _params);
    }
    _ImageLayer.prototype = Object.create(esri.layers.ArcGISImageServiceLayer.prototype);

    // _TiledLayer:
    function _TiledLayer(_url, _params) {
        esri.layers.ArcGISTiledMapServiceLayer.call(this, _url, _params);
    }
    _TiledLayer.prototype = Object.create(esri.layers.ArcGISTiledMapServiceLayer.prototype);
    
    // _AirphotoLayer: extend ArcGISDynamicMapServiceLayer to include a various raster catalog
    // manipulation functions
    function _AirphotoLayer(_url, _params) {
        esri.layers.ArcGISDynamicMapServiceLayer.call(this, _url, _params);
        this.setVisibleLayers([0]);
    }
    _AirphotoLayer.prototype = Object.create(esri.layers.ArcGISDynamicMapServiceLayer.prototype);
    _AirphotoLayer.prototype.isAirphoto = true;
    _AirphotoLayer.prototype.displayedTiles = [];
    _AirphotoLayer.prototype._arrayToRange = function (arr) {
        var str = "(";
        for (var i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                str += ("'" + arr[i] + "'");
            }
            else {
                str += ("'" + arr[i] + "',");
            }
        }
        str += ")";
        return str;
    }
    _AirphotoLayer.prototype.setPhotos = function (array) {                       
        this.displayedTiles = array; // just set the object's internal list of tiles. it's good to know.                       
        var range = this._arrayToRange(array);  // the string magic
        // note that we're implicitly pushing to index [0] because that's the only layer within the servce. if we had other layers
        // and/or the tiles weren't in [0], we couldn't do this - we'd have to actually supply the index.
        var layerDefinitions = [];
        layerDefinitions.push("Name IN " + range);
        this.setLayerDefinitions(layerDefinitions, false);
    }

    reha.layers = {
        // IMPORTANT - this is a list of layer models on the map
        Layers: [],  
        // 
        isBase: function (url) {
            //TODO - complete this
        },
        // is this an image service layer
        isImage: function (url) {
            return url.substr(url.lastIndexOf("/")) === "ImageServer";
        },
        // is this a map service layer?
        isMap: function () {
            return url.substr(url.lastIndexOf("/")) === "MapServer";
        },
        // remove layer from Layers array - not from the map
        removeLayer: function(/*String*/ name) {
            $.each(this.Layers, $.proxy(function (i, o) {
                if (name == o.name) {
                    this.Layers.splice(i, 1);
                    return false;
                }
            }, this));
        },
        // note that the single arg is an object containing properties from "/handlers/menu.ashx"
        Layer: function (/*String*/ props, /*Boolean*/ force, /*String*/ forceType) {
            var layer = null;
            // first figure out which layers are supported
            var isImage = $.inArray("image", props.supportedLayers) > -1;
            var isTiled = $.inArray("tiled", props.supportedLayers) > -1;
            var isDynamic = $.inArray("dynamic", props.supportedLayers) > -1;
            var isAirphoto = $.inArray("airphoto", props.supportedLayers) > -1;
            // if someone has included a "force" flag, they want to ensure the type 
            // of layer indicated. make sure that happens.  
            if (force) {
                switch (forceType) {
                    case "image":
                        isImage = true;
                        break;
                    case "tiled":
                        isTiled = true;
                        break;
                    case "dynamic":
                        isDynamic = true;
                        break;
                    case "airphoto":
                        isAirphoto = true;
                        break;
                    default: 
                        isDynamic = true; 
                }
            }
            // now build the layer appropriately
            if (isImage) {
                madgic.util.log("...creating image layer");
                layer = new _ImageLayer(props.url, {
                    id: props.title,
                    name: props.name,
                    visible: true
                });
            } else if (isAirphoto) {
                madgic.util.log("...creating airphoto layer");
                layer = new _AirphotoLayer(props.url, {
                    "id": props.name,
                    "name": props.name,
                    "visible": true
                });
                layer.setPhotos([]);
            } else if (isTiled) {
                madgic.util.log("...creating tiled layer");
                layer = new _TiledLayer(props.url, {
                    id: props.title,
                    name: props.name,
                    visible: true
                });
            } else if (!isTiled && isDynamic) {
                madgic.util.log("...creating dynamic layer");
                layer = new _DynamicLayer(props.url, {
                    id: props.title,
                    name: props.name,
                    visible: true,
                    opacity: (props.title === "REHA Boundary") ? 0.45 : 1 // exception for REHA boundary!
                });
            } else {
                // it wasn't one of the normal layers - what's up?
                madgic.util.log("Error: cannot build layer type. Layer type is not supported.");
                throw "Error: cannot build layer type. Layer type is not supported.";
            }
            return layer;
        }
    }
})(jQuery);