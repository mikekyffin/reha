// file:            identify.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
(function ($) {
    reha.identify = {
        // an array of layers to identify
        IdentifyList: function (/*esri.Map*/ map) {
            return $.map(map.layerIds, function (name, idx) {
                return (idx != 0) ? {
                    "id": map.getLayer(map.layerIds[idx]).id,
                    "url": map.getLayer(map.layerIds[idx]).url
                } : null;
            });
        },

        identify: function (/*esri.Map*/ map, /*Double*/ x, /*Double*/ y, /*reha.identify.IdentifyList*/ list) {
            map.infoWindow.clearFeatures();
            var tasks = [], results = [];
            // this is for ga tracking 
            var geographic = esri.geometry.webMercatorToGeographic(new esri.geometry.Point(x, y, new esri.SpatialReference({ "wkid": 102100 })));
            // 
            $.each(list, function (idx, service) {
                var ident = new madgic.esri.Identify(service.url, reha.config.PROXY, true);
                tasks.push(ident.doIdentify(0, x, y, map.spatialReference.wkid, map.extent));
                reha.ga.track({
                    "eventCategory": "Identify",
                    "eventAction": service["id"],
                    "eventLabel": geographic["y"] + ", " + geographic["x"],
                    "eventValue": null
                });
            });
            // queue up the async tasks
            // TODO - error handling
            var multiXhr = $.when.apply(null, tasks).done($.proxy(function () {
                // deal with the argument object and all its funkiness
                var finds = (arguments.length > 0 && !$.isArray(arguments[0])) ? finds = [arguments] : Array.prototype.slice.call(arguments);

                $.each(finds, $.proxy(function (idx, val) {
                    // check to see if it's a return from a MapService
                    if (val[0].results) {
                        // add the layer id so it can be displayed in the popup later.   
                        var withId = this._addServiceId(val[0].results, list[idx].id);
                        results = results.concat(withId);
                    // check to see if it's a return from an ImageService
                    } else if (val[0] && val[0].name === "Pixel") {
                        // this is an image service.
                        // convert to a more-vector style result so it can displayed seamlessly in the popup.
                        // add the layer id so it can be displayed in the popup later.   
                        var result = this._imageResultToResult(val[0], list[idx].id);
                        results = results.concat(result);
                    } else {
                        // noop
                    }
                }, this));
                
                // TODO - we need a callback
                // assign the features from all the layers to the popoup
                map.infoWindow.setFeatures(this._graphicsFromResults(results));
                // this doesn't belong here!
                $(".actionsPane").removeClass("hidden");
                $(".actionList").removeClass("hidden");
                if (results.length > 0) {
                    map.centerAt(new esri.geometry.Point(x, y, map.spatialReference));
                }
            }, this));

            //.error(function (err) {
            //    // TODO: make this not suck
            //    madgic.util.log("error: something terrible happened with array of deffered identify calls.");
            //    madgic.util.log(err);
            //});
        },

        _addServiceId: function(/*Object*/ results, /*String*/ id) {
            return $.each(results, function (i, val) {
                val["_id_"] = id;
            });
        },

        _createInfoTemplate: function (/*Object*/ attributes, /*String*/ title) {
            var str = "<table style=\"width: 100%\">";
            str += "<tr class=\"iw-title-header\"><td colspan=\"2\"><span class=\"iw-title\">Layer: " + title + "</span></td></tr>";
            for (var key in attributes) {
                str += "<tr class=\"iw-row\"><td><span class=\"iw-text iw-bold\">" + key + "</span></td><td><span class=\"iw-text\">" + attributes[key] + "</span></td></tr>";
            };
            str += "</table>";
            return new esri.InfoTemplate("Attributes", str);
        },

        _imageResultToResult: function (iResult, id) {
            return {
                _id_: id,
                isImage: true,
                //geometryType: "esriGeometryPoint",
                //geometry: '',
                location: iResult.location,
                attributes: {
                    "Pixel Value": iResult["value"]
                }
            }
        },

        _graphicsFromResults: function (/*Array*/ arr) {
            var lineColour = new dojo.Color([98, 194, 204]); // as RGB
            var lineWidth = 2;
            var fillOpacity = 0.5;
            var fillColour = new dojo.Color([98, 194, 204, fillOpacity]); // as RGB+Opacity
            var symbol = esri.symbol.SimpleFillSymbol(
					esri.symbol.SimpleFillSymbol.STYLE_SOLID, // style
					new esri.symbol.SimpleLineSymbol(lineColour, lineWidth), // the outline
					fillColour // the fill
			);
            return $.map(arr, $.proxy(function (val) {
                return new esri.Graphic(
                    (val.isImage)
                        ? new esri.geometry.Point(val.location.x, val.location.y, new esri.SpatialReference({ wkid: 102100 }))
                        : new esri.geometry.Polygon(val.geometry),
                    symbol,
                    val.attributes,
                    this._createInfoTemplate(val.attributes, val._id_)
                );
            }, this));
        }
    }
})(jQuery);