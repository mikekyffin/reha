// file:            models.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!


var reha = reha || {};
(function ($) {
    reha.models = {
        // the model is a mixin of the layer on the map, the view and the service properties (metadata).
        LayerModel: function (_layer, _view, _specs) {
            $.extend(this, _specs);
            this.layer = _layer;
            this.view = _view;
            if (this.layer.isAirphoto) { 
                this.type = "Air Photo Service";
            }
            return this;
        }
    }
})(jQuery);