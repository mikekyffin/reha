// file:            behaviours.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
(function ($) {
    // TODO - any way we can reduce this?
    reha.behaviours = {
        // initialize any general DOM behaviours not already internal to specific objects/elements        
        init: function () {
            //
            $("#title").click(function () {               
                reha.ga.track({
                    "eventCategory": "Link",
                    "eventAction": "click",
                    "eventLabel": "http://trentu.ca/library/madgic/reha.htm",
                    "eventValue": null
                });
                window.open("http://trentu.ca/library/madgic/reha.htm", "_blank");
            });
            // open and close the right content pane - from the little arrow toggle
            $("#toggle").bind("click", function () {
                if ($("#right").hasClass("menuOpen")) {
                    $("#right").addClass("menuClosed");
                    $("#right").removeClass("menuOpen");
                    $("#arrow").addClass("arrow-left");
                    $("#arrow").removeClass("arrow-right");
                    dijit.byId("map").resize();
                    dijit.byId("bc").resize();
                    reha.ga.track({
                        "eventCategory": "Toggle",
                        "eventAction": "click",
                        "eventLabel": "menu close",
                        "eventValue": null
                    });

                } else {
                    $("#right").addClass("menuOpen");
                    $("#right").removeClass("menuClosed");
                    $("#arrow").addClass("arrow-right");
                    $("#arrow").removeClass("arrow-left");
                    dijit.byId("map").resize();
                    dijit.byId("bc").resize();
                    reha.ga.track({
                        "eventCategory": "Toggle",
                        "eventAction": "click",
                        "eventLabel": "menu open",
                        "eventValue": null
                    });
                }
            });

            // open the Airphoto portion of the REHA web presence
            //$("#apSelector").click(function () {
            //    window.open("http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "madgic.trentu.ca") + "/airphoto");
            //    reha.ga.track({
            //        "eventCategory": "Link",
            //        "eventAction": "click",
            //        "eventLabel": "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "madgic.trentu.ca") + "/airphoto",
            //        "eventValue": null
            //    });            
            //});

            // open the about page in the dialog
            $("#aboutSelector").click(function () {
                reha.ui.setDialogFromAsyncHtml("about.html", "About");
                reha.ui.openDialog();
                reha.ga.track({
                    "eventCategory": "Link",
                    "eventAction": "click",
                    "eventLabel": "about.html",
                    "eventValue": null
                });
            });
            // open the help page in the dialog
            $("#helpSelector").click(function () {
                reha.ui.setDialogFromAsyncHtml("help.html", "Help");
                reha.ui.openDialog();
                reha.ga.track({
                    "eventCategory": "Link",
                    "eventAction": "click",
                    "eventLabel": "help.html",
                    "eventValue": null
                });
            });
            // empties the list of search views
            $("#searchClear").click(function () {
                reha.ui.clearSearch();
            });
            //
            $("#downloadClear").click(function () {
                reha.ui.clearDownload();
            });
            //
            $(".mode-button").click(function () {
                $(".mode-button").removeClass("enabled");
                $(".mode-button").addClass("disabled");
                $(this).removeClass("disabled");
                $(this).addClass("enabled");
                reha.ui.setMode($(this).data("value"));
                reha.ga.track({
                    "eventCategory": "Toggle",
                    "eventAction": "click",
                    "eventLabel": $(this).data("value"),
                    "eventValue": null
                });
            });

            //create a notifaction area at the node provided
            $.notify.init($("#notify"));
        }
    }
})(jQuery);