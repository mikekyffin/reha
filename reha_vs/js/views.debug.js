// file:            views.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
reha.views = reha.views || {};
//TODO - need to move more styles to main.less
(function ($) {
    // main view for each layers coming from ServiceDefinitions.ashx
    reha.views.LayerView = function (/*esri.layers.Layer*/ layer, /*Object*/ props, /*Object*/ callbacks) {
        this.layer = layer;
        this.callbacks = callbacks;
        // regex some non-standard characters out
        var name = props.title.replace(/[\W\s]/g, "_");
        // we'll need the url for various things later, so might as well keep the object handy
        this.data = props;
        //the parent container of the view
        this.view = $("<li>", {
            "id": "layerItem_" + name,
            "layer": props.title,
            "style": "display:none;",
            "class": "view"
        });
        // the display title of the service
        var title = $("<div>", {
            "style": "display:block",
            "html": props.title,
            "class": "view-title"
        }).appendTo(this.view);

        // zoom to button
        var zoom = $("<div>Zoom</div>")
            .button({
                icons: {
                    primary: "ui-icon-circle-zoomin"
                }
            })
            .click(function () {
                callbacks.zoomToLayer(layer);
            })
            .appendTo(this.view);
        // remove from map button     
        var remove = $("<div>Remove</div>")
            .button({
                icons: {
                    primary: "ui-icon-circle-close"
                }
            })
            .click($.proxy(function () {
                callbacks.removeLayer(layer, props.name);
                // the ui part is internal to the view anyway. fadeOut - onComplete remove it entirely from the DOM
                this.destroy();

            }, this))
            .appendTo(this.view);
        //opacity slider
        var opacity = $("<div>")
            .slider({
                min: 0,
                max: 100,
                value: 100,
                slide: function () {
                    layer.setOpacity($(this).slider("value") / 100);
                },
                stop: function (evt) {
                    callbacks.onOpacityChange(props, $(this));
                }
            })
                .addClass("view-opacity-slider")
                .appendTo(this.view);



        // make a wrapper div to hold all the toggles
        var toggleContainer = $("<div>", {
            "id": "toggleContainer_" + name,
            "style": "display:block",
            "class": "view-toggle-container"
        }).appendTo(this.view);           
        // toggles for metadata material
        var summaryToggle = $("<div>", {
            "id": "summaryToggle_" + name,
            "data-meta-type": "summary",
            "html": "<span>Summary<span id=\"summaryIcon_" + name + "\" class=\"ui-icon ui-icon-triangle-1-e view-arrow\" style=\"display:inline-block;vertical-align:middle;\"></span></span>",
            "class": "view-toggle"
        }).appendTo($(toggleContainer));
        summaryToggle.click(function () {
            var displaySelector = "#" + this.id.replace("Toggle", "");
            var iconSelector = "#" + this.id.replace("Toggle", "Icon");
            callbacks.toggle(displaySelector, iconSelector, $(this).data("meta-type"), props["title"]);
        });
        //    
        var creditToggle = $("<div>", {
            id: "creditToggle_" + name,
            "data-meta-type": "credit",
            html: "<span>Credit<span id=\"creditIcon_" + name + "\" class=\"ui-icon ui-icon-triangle-1-e\" style=\"display:inline-block;vertical-align:middle;\"></span></span>",
            "class": "view-toggle"
        }).appendTo($(toggleContainer));
        creditToggle.click(function () {
            var displaySelector = "#" + this.id.replace("Toggle", "");
            var iconSelector = "#" + this.id.replace("Toggle", "Icon");
            callbacks.toggle(displaySelector, iconSelector, $(this).data("meta-type"), props["title"]);
        });
        //    
        var commentsToggle = $("<div>", {
            "id": "commentsToggle_" + name,
            "data-meta-type": "comments",
            "html": "<span>Comments<span id=\"commentsIcon_" + name + "\" class=\"ui-icon ui-icon-triangle-1-e\" style=\"display:inline-block;vertical-align:middle;\"></span></span>",
            "class": "view-toggle"
        }).appendTo($(toggleContainer));
        commentsToggle.click(function () {
            var displaySelector = "#" + this.id.replace("Toggle", "");
            var iconSelector = "#" + this.id.replace("Toggle", "Icon");
            callbacks.toggle(displaySelector, iconSelector, $(this).data("meta-type"), props["title"]);
        });
        
        //
        var metadataContainer = $("<div>", {
            "id": "metadataContainer_" + name,
            "class": "view-metadata-containter"
        }).appendTo(this.view);            
        //
        var summary = $("<div>", {
            "id": "summary_" + name,
            "style": "display:none;",
            "html": "<b>Summary: </b>" + ((!props.summary.trim()) ? props.summary : ""),
            "class": "view-metadata"
        }).appendTo($(metadataContainer));
        //    
        var credit = $("<div>", {
            "id": "credit_" + name,
            "style": "display:none;",
            "html": "<b>Credit: </b>" + ((!props.credits.trim()) ? props.credits : ""),
            "class": "view-metadata"
        }).appendTo($(metadataContainer));
        //
        var comments = $("<div>", {
            "id": "comments_" + name,
            "style": "display:none;",
            "html": "<b>Comments: </b>" + ((!props.description.trim()) ? props.description : ""),
            "class": "view-metadata"
        }).appendTo($(metadataContainer));            

        // if this thing is a MapServer service, then it has a legend. 
        // go get that and make a place in the view for it.
        if (this.data.url.match(/.*MapServer$/)) {
            var legendToggle = $("<div>", {
                "id": "legendToggle_" + name,
                "data-meta-type": "legend",
                "html": "<span>Legend<span id=\"legendIcon_" + name + "\" class=\"ui-icon ui-icon-triangle-1-e\" style=\"display:inline-block;vertical-align:middle;\"></span></span>",
                "class": "view-toggle"
            }).appendTo($(toggleContainer));
            legendToggle.click(function () {
                var displaySelector = "#" + this.id.replace("Toggle", "");
                var iconSelector = "#" + this.id.replace("Toggle", "Icon");
                callbacks.toggle(displaySelector, iconSelector, $(this).data("meta-type"), props["title"]);
            });
            var legend = $("<div>", {
                "id": "legend_" + name,
                "style": "display:none;",
                "class": "view-metadata view-legend"
            }).appendTo($(metadataContainer));

            var legendXhr = this._getLegend();
            legendXhr.done(function (resp) {
                this._renderLegend(resp, this.data.url, legend);
            }.bind(this));
        }

        this._checkDynamicLayers(layer.url, this._addSymbologyUI.bind(this),function () { });
    }

    reha.views.LayerView.prototype._addSymbologyUI = function () {
        $("<span>", {
            "html": "<i class=\"fa fa-pencil-square-o\"></i>",
            "style": "cursor:pointer;margin: 0 0 0 2px;",
            "click": function () {
                this.callbacks.openSymbologyDialog(this.layer, this);
            }.bind(this)
        }).appendTo($(".view-title", this.view));
    },
    // async
    reha.views.LayerView.prototype._checkDynamicLayers = function (url, callback, errback) {
        var xhr = $.getJSON(reha.config.PROXY + "?" + url + "?f=json");
        xhr.done(function (resp) {
            if (resp.supportsDynamicLayers) {
                callback();
            }
        });
        xhr.fail(function () {
            errback();
        });
    }
    // place in DOM
    reha.views.LayerView.prototype.place = function (node) {
        this.view.prependTo(node);
        this.view.fadeIn(400, function () {
            $(this).trigger("view-create"); // fire the on-create event to that listeners know the view DOM is ready
        }.bind(this));
    }
    //
    reha.views.LayerView.prototype._renderLegend = function (data, baseUrl, node) {
        node.empty();
        $.each(data.layers, function (i, layer) {
            $.each(layer.legend, function (n, elem) {
                var _item = $("<div>").appendTo(node);
                var _patch = $("<img>", {
                    "src": baseUrl + "/" + i + "/images/" + elem.url,
                    "style": "vertical-align:middle;"
                }).appendTo(_item);
                var _label = $("<span>", {
                    "html": elem.label,
                    "style": "padding: 2px 0 0 4px"
                }).appendTo(_item);                
            });
        });
    }

    reha.views.LayerView.prototype.setLegendFromService = function () {
        var xhr = this._getLegend();
        xhr.done(function (resp) {
            this._renderLegend(resp, this.data.url, $(".view-legend", this.view));
        }.bind(this));
    }

    reha.views.LayerView.prototype.setLegendFromRenderer = function (renderer) {
        var node = $(".view-legend", this.view);
        var renderedLegend = $("<div>", {
            "class": "view-legend-rendered"
        });
        $.each(renderer.infos, function (i, item) {
            var legendItem = $("<div>", {
                style: "margin: 0 0  2px 0;"
            });
            $("<div>", {
                style: "float:left;height:19px;width:19px;vertical-align: middle;"
            })
                .css("background-color", new dojo.Color(item.symbol.color).toHex())
                .css("border", "1px solid " + new dojo.Color(item.symbol.outline.color).toHex())
                .appendTo(legendItem);
            $("<div>", {
                html: item.label ? item.label : item.value,
                style: "float:left;padding: 2px 0 0 4px;"
            }).appendTo(legendItem);
            $("<div>", { style: "clear:both;" }).appendTo(legendItem);
            legendItem.appendTo(renderedLegend);
        });
        node.html(renderedLegend);

    }

    // asynchronous - calls the main, unclassified legend as defined by the service
    reha.views.LayerView.prototype._getLegend = function () {
        return $.getJSON(reha.config.PROXY + "?" + this.data.url + "/legend" + "?" + $.param({
            "f": "json"
        }));
    }
    // remove from DOM
    reha.views.LayerView.prototype.destroy = function () {
        this.view.fadeOut({
            complete: function () {
                this.view.remove();
            }.bind(this)
        });
    }

    /*
    * the view from a returned location query.
    * take the extent values from the geolocation api (in lat long!) repsonse and send them to the geometry server for projection.
    * use these projcted values to build an extent and zoom the map to it. note that because we're using an extent, we can get small and/or
    * large features. for instance - compare peterborough (city), peterborough (county) and omemee.
    * see: http://help.arcgis.com/en/arcgisserver/10.0/apis/rest/project.html and http://help.arcgis.com/en/arcgisserver/10.0/apis/rest/geometry.html
    */
    reha.views.LocationView = function (/*Object*/ place, /*Function*/ callback) {
        // node to hold everything
        this.view = $("<div>", {
            style: "display:none;"
        }).addClass("locationView");
        // name of location
        var _name = $("<div>", {
            html: place.name
        }).addClass("locName");
        // label
        var _label = $("<div>", {
            style: "clear:both;",
            html: place.label
        }).addClass("locLabel");
        //
        var _gotoButton = $("<div>Go to!</div>")
            .button({
                icons: {
                    primary: "ui-icon-circle-triangle-e"
                }
            })
            .addClass("locButton")
            .click(function (evt) {
                callback(place, true);
            });
        //
        this.view.append(_name);
        this.view.append(_label);
        this.view.append(_gotoButton);
        return this;
    };
    // place in DOM
    reha.views.LocationView.prototype.place = function (/*domNode*/ node) {
        node.prepend(this.view);
        $(this.view).fadeIn();
    };
    // remove from DOM
    reha.views.LocationView.prototype.destroy = function () {
        $(this.view).remove();
    };

    /* */
    reha.views.DataView = function (/*Object*/ result, /*Function*/ callback) {
        // node to hold every
        this.domNode = $("<div>", {
            style: "display:none;"
        }).addClass("locationView");

        // just the name, really
        var _name = $("<div>", {
            html: result.title
        })
            .appendTo(this.domNode)
            .addClass("dataTitle");

        // the "add result to the map button"
        var _addButton = $("<div>Add to map</div>")
            .appendTo(this.domNode)
            .button({
                icons: {
                    primary: "ui-icon-circle-plus"
                }               
            })
            .addClass("search-button")
            .click(function (evt) {
                // callback is the addLayer function
                callback(result.menuItem);               
            });
        
        // tidy (third-person singular simple present tidies, present participle tidying, 
        // simple past and past participle tidied).
        //      1. To make tidy; to neaten.
        var _clear = $("<div>", {
            style: "clear:both;"
        }).appendTo(this.domNode);
    }
    // place it in the DOM
    reha.views.DataView.prototype.place = function(/*DomNode*/ node) {
        node.prepend(this.domNode);
        this.domNode.fadeIn();
    }
    // remove it
    reha.views.DataView.prototype.destroy = function() {
        this.domNode.remove();
    }

    // TODO - add styles as css classes
    /* */
    reha.views.DownloadView = function (/*Object*/ obj) {
        this.model = obj;
        // container div for the view
        this.view = $("<div>")
            .addClass("download-view");
        // top row

        var _row1 = $("<div>")
            .appendTo(this.view);
        // top row contents
        var _ul = $("<div>", {
            "class": "download-view-icon"
        }).appendTo(_row1);

        this.spinner = $("<img>", {
            "class": "download-view-spinner",
            "src": "images/gear_5F89AA_FEFEFE_16x16.gif"
        }).appendTo(_ul);

        var _ur = $("<div>", {
            html: obj.title,
            "class": "download-view-text"
        }).appendTo(_row1);

        // bottom row
        var _row2 = $("<div>")
            .appendTo(this.view);

        var _ll = $("<div>", {
            "class": "download-view-icon"
        }).appendTo(_row2);

        this.info = $("<span>")
            .appendTo(_ll);

        var _lr = $("<div>", {
            "class": "download-view-text"
        }).appendTo(_row2);

        this.message = $("<span>", {
            "html": "Initializing download..."
        }).appendTo(_lr);
    }
    // can be "info", "error" or "complete"
    reha.views.DownloadView.prototype.statusIcon = function(/*String*/ str){
        switch (str) {
            case "info":
                this.info.removeClass();
                this.info.addClass("ui-icon ui-icon-info");
                break;
            case "error":
                this.info.removeClass();
                this.info.addClass("ui-icon ui-icon-alert");
                break;
            case "complete":
                this.info.removeClass();
                this.info.addClass("ui-icon ui-icon-circle-check");
                break;
            default:
                this.info.removeClass();
                this.info.addClass("ui-icon ui-icon-info");
        }
    }
    // update the message in the view
    reha.views.DownloadView.prototype.addMessage = function (/*String or DomNode*/ message) { 
        this.message.html(message);
    }
    // place in DOM
    reha.views.DownloadView.prototype.place = function (node) { 
        node.prepend(this.view);
        this.view.fadeIn();
    }
    // remove
    reha.views.DownloadView.prototype.destroy = function () { 
        this.view.remove();
    }
    // ui bahaviours for download error
    reha.views.DownloadView.prototype.onError = function (msg) {
        this.spinner.css("display", "none");
        this.statusIcon("error");
        this.addMessage(msg);
    }
    // completion of download operation - provide proxied link
    // TODO - get year of topo or provide 0
    reha.views.DownloadView.prototype.onComplete = function (data) {
        this.spinner.css("display", "none");
        this.statusIcon("complete");
        var link = $("<a>", {
            //href: data.value.url,
            href: reha.config.DOWNLOAD_AUTH + reha.config.DOWNLOAD_DISCLAIMER + "?" + $.param({
                "proj": "reha",
                "year": 0, //test! 
                "file": data.value.url
            }),
            html: "Click to download",
            target: "_blank"
        });
        this.addMessage(link);
    }

    reha.views.AirphotoView = function (layer, /*Object*/ props, callbacks) {

        this.view = $("<li>", {
            "id": "layerItem_" + props.title,
            "layer": props.name,
            "style": "display:none;position:relative;",
            "class": "view view-airphoto"
        });

        var title = $("<div>", {
            "html": props.title.substring(1) + " Air photos (" + props.photos.length + ")",
            "class": "view-title",
            "style": "float: left;"
        }).appendTo(this.view);

        this.open = $("<div>", {
            "html": $("<i class=\"fa fa-plus-square\"></i>"),
            "class": "view-airphoto-icon",
            "style": "display:none;",
            "click": function () {
                this.photoContainer.css("display", "block");
                this.close.css("display", "block");
                this.open.css("display", "none");
            }.bind(this)
        }).appendTo(this.view);

        this.close = $("<div>", {
            "html": $("<i class=\"fa fa-minus-square\"></i>"),
            "class": "view-airphoto-icon",
            "click": function () {
                this.photoContainer.css("display", "none");
                this.close.css("display", "none");
                this.open.css("display", "block");
            }.bind(this)
        }).appendTo(this.view);

        $("<div>", {
            "style": "clear:both;"
        }).appendTo(this.view);

        this.overlayContainer = $("<div>", {
            "html": $("<img>", { src: "images/gear_FEFEFE_5F89AA_28x28.gif"}),
            "style": "text-align:center;display:none;"
        }).appendTo(this.view);

        this.photoContainer = $("<div>", {
            "class": "view-airphoto-photo-container"
        }).appendTo(this.view);

        // the photos!!!
        $.each(props.photos, function (i, photo) {
            $("<div>", {
                "class": "view-airphoto-photo",
                html: photo.value.substring(0, photo.value.lastIndexOf(".")),
                click: function (evt) {
                    this._clearCheck();
                    this.update(layer);
                    layer.setPhotos([photo.value]);
                    this._addCheck($(evt.currentTarget));
                }.bind(this)
            }).appendTo(this.photoContainer);
        }.bind(this));

        $("<div>", {
            "style": "clear:both;"
        }).appendTo(this.view);

        // zoom to button
        var zoom = $("<div>Zoom</div>")
            .button({
                icons: {
                    primary: "ui-icon-circle-zoomin"
                }
            })
            .click(function () {
                callbacks.zoomToLayer(layer);
            })
            .appendTo(this.view);
        // remove from map button     
        var remove = $("<div>Remove</div>")
            .button({
                icons: {
                    primary: "ui-icon-circle-close"
                }
            })
            .click($.proxy(function () {
                callbacks.removeLayer(layer, props.name);
                // the ui part is internal to the view anyway. fadeOut - onComplete remove it entirely from the DOM
                this.view.fadeOut({
                    complete: $.proxy(function () {
                        this.view.remove();
                    }, this)
                });

            }, this))
            .appendTo(this.view);
        //opacity slider
        var opacity = $("<div>", {
            "class": "view-opacity-slider"
        }).slider({
            min: 0,
            max: 100,
            value: 100,
            slide: function () {
                layer.setOpacity($(this).slider("value") / 100);
            },
            stop: function (evt) {
                callbacks.onOpacityChange(props, $(this));
            }
        }).appendTo(this.view);
    }

    reha.views.AirphotoView.prototype._addCheck = function (node) {
        node.prepend($("<i>", { "class": "fa fa-check" }));
    }

    reha.views.AirphotoView.prototype._clearCheck = function () {
        $(".fa.fa-check", this.view).remove();
    }

    // place in DOM
    reha.views.AirphotoView.prototype.place = function (node) {
        this.view.prependTo(node);
        this.view.fadeIn(400, function () {
            $(this).trigger("view-create"); // fire the on-create event to that listeners know the view DOM is ready
        }.bind(this));
    }

    reha.views.AirphotoView.prototype.update = function (layer) {
        this._onUpdateStart();
        var hook = dojo.connect(layer, "onUpdateEnd", this, function () {
            this._onUpdateEnd();
            dojo.disconnect(hook);
        });
    }

    reha.views.AirphotoView.prototype._onUpdateEnd = function () {
        this.overlayContainer.fadeOut(1);
        this.photoContainer.fadeIn(1);

    }

    reha.views.AirphotoView.prototype._onUpdateStart = function () {
        this.overlayContainer.fadeIn(1);
        this.photoContainer.fadeOut(1);
    }

    // remove from DOM
    reha.views.AirphotoView.prototype.destroy = function () {
        this.view.fadeOut({
            "complete": function () {
                this.view.remove();
            }.bind(this)
        });        
    }

})(window.jQuery);