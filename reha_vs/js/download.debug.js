// file:            download.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
(function ($) {
    reha.download = {
        //
        VectorDownload: function (val) {

            var _view = new reha.views.DownloadView(val);
            _view.place($("#downloadList"));
            _view.statusIcon("info");
            
            var _gp = new esri.tasks.Geoprocessor(reha.config.DOWNLOAD_VECTOR_URL);

            var _jobComplete = function (jobInfo) {
                if (jobInfo.jobStatus !== "esriJobFailed") {
                    _gp.getResultData(jobInfo.jobId, "Output", function (data) {
                        _view.onComplete(data);
                    }, function (err) {

                    });
                }
            };

            var _jobStatus = function (jobInfo) {
                if (jobInfo.messages.length > 0) {
                    _view.addMessage(jobInfo.messages[jobInfo.messages.length - 1].description);
                }
            };

            var _jobError = function (jobInfo) {
                console.log(jobInfo);
            };

            return {
                submitJob: function () {
                    _gp.submitJob({
                        Xmin: val.xmin,
                        Xmax: val.xmax,
                        Ymin: val.ymin,
                        Ymax: val.ymax,
                        Dataset_Name: val.name,
                        Project_Name: "reha",
                        f: "json"
                    }, _jobComplete, _jobStatus, _jobError);
                }
            };
        },

        AirPhotoDownload: function (val) {

            var _view = new reha.views.DownloadView(val);
            _view.place($("#downloadList"));
            _view.statusIcon("info");

            var _gp = new esri.tasks.Geoprocessor(reha.config.DOWNLOAD_AIR_PHOTO_URL);
            var _jobStatus = function (jobInfo) {
                if (jobInfo.messages.length > 0) {
                    _view.addMessage(jobInfo.messages[jobInfo.messages.length - 1].description);
                }
            };

            var _jobError = function (jobInfo) {
                // TODO - get message from jobInfo
                _view.onError("Error!");
            };

            var _jobComplete = function (jobInfo) {
                if (jobInfo.jobStatus !== "esriJobFailed") {
                    _gp.getResultData(jobInfo.jobId, "Output", function (data) {
                        _view.onComplete(data);
                    }, function (err) {
                        //TODO
                    });
                }
            };
            return {
                submitJob: function () {
                    val.layer.displayedTiles.forEach(function (item) {
                        var params = {
                            "Tile": item.replace(".tif", ""),
                            "Project": "airphoto/" + val.name,
                            "f": "json"
                        }
                        _gp.submitJob(params, _jobComplete, _jobStatus, _jobError);
                    });
                }
            }
        },
        //
        RasterDownload: function (val) {

            var _view = new reha.views.DownloadView(val);
            _view.place($("#downloadList"));
            _view.statusIcon("info");

            var _gp = new esri.tasks.Geoprocessor(reha.config.DOWNLOAD_RASTER_URL);

            var _jobStatus = function (jobInfo) {
                if (jobInfo.messages.length > 0) {
                    _view.addMessage(jobInfo.messages[jobInfo.messages.length - 1].description);
                }
            };

            var _jobError = function (jobInfo) {
                // TODO - get message from jobInfo
                _view.onError("Error!");
            };

            var _jobComplete = function (jobInfo) {
                if (jobInfo.jobStatus !== "esriJobFailed") {
                    _gp.getResultData(jobInfo.jobId, "Output", function (data) {
                        _view.onComplete(data);
                    }, function (err) {
                        //TODO
                    });
                }
            };

            var _queryByEnvelope = function () {
                return $.getJSON(reha.config.SERVICE_BASE + "/" + val.name + "/ImageServer/query?" + $.param({
                    f: "json",
                    where: "1=1",
                    geometry: val.xmin + "," + val.ymin + "," + val.xmax + "," + val.ymax,
                    returnGeometry: true,
                    geometryType: "esriGeometryEnvelope",
                    outFields: "*",
                    spatialRel: "esriSpatialRelIntersects",
                    returnCountOnly: false,
                    returnIdsOnly: false
                }));
            };
            // end of closure
            return {
                submitJob: function () {
                    var getTiles = _queryByEnvelope();
                    // TODO: need better error handling here!
                    getTiles.done(function (resp) {
                        // if resp.features == 0, then the query geometry doesn't 
                        // overlap the map/raster/service.    
                        // don't submit a pointless request!
                        if (resp.features && resp.features.length > 0) {
                            $.each(resp.features, function (i, obj) {
                                if (obj["attributes"]["Category"] === 1) {
                                    /*
                                    @Tile changes to @Map to provide proper parameter name for GetMap servive
                                    */
                                    var params = {
                                        "Map": obj["attributes"]["Name"],
                                        "Project": "reha",
                                        "f": "json"
                                    }
                                    _gp.submitJob(params, _jobComplete, _jobStatus, _jobError);
                                } else {
                                    madgic.util.log("...skipping overview tile. Cannot download overviews. This is intentional.");
                                }
                            });
                        } else {                           
                            _view.onError("Warning: download area does not intersect the resource.");
                        }
                    });
                }
            }
        }
    }
})(jQuery);