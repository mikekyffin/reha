// file:            widgets.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var reha = reha || {};
reha.widgets = reha.widgets || {};
(function ($) {
    /**
    * allows the creation of a drop down panel in the DOM to be used in a top menu bar.
    * set content as HTML.
    */
    reha.widgets.MenuPanel = function (/*String*/ placeholderName, /*domNode*/ toggle) {
        //figure the location and create an internal position obj
        var _x = $(toggle).offset().left;
        var _y = $(toggle).offset().top + $(toggle).height();

        this._pos = {
                    x: _x + "px",
                    y: _y + "px"
                };
        //the parent div to hold all the panel contents
        this.anchor = $("<div>")
            .addClass("menu-anchor menu-panel")
            //.css("background-color", "#F2F5F7");
             //a loading spinner holding thing for panel that are to include AJAX content
        this.loader = $("<div>", {
            style: "display:none",
            html: $("<img>", {
                src: "images/gear_5F89AA_FEFEFE_64x64.gif"
            })
        }).appendTo(this.anchor);
        this.content = $("<div>", {
            html: ""
        }).appendTo(this.anchor);
        //add a click handler to the suplied toggle node to hide/show the MenuPanel
        toggle.click($.proxy(function () {
            if (this.anchor.css("display") == "none") {
                $(".menu-anchor").css("display", "none");
                this.anchor.css("display", "block");
            } else {
                this.anchor.css("display", "none");
            }
        }, this));
        return this;
    };
    //
    reha.widgets.MenuPanel.prototype.place = function () {
        this.anchor.css({
            "position": "absolute",
            "top": this._pos.y,
            "left": this._pos.x
        }).appendTo($("body"));
    };
    //
    reha.widgets.MenuPanel.prototype.show = function () {
        this.anchor.css({ "display": "block" });
    };
    //
    reha.widgets.MenuPanel.prototype.hide = function () {
        this.anchor.css({ "display": "none" });
    };
    //
    reha.widgets.MenuPanel.prototype.hideAll = function () {
        $(".menu-anchor").css("display", "none");
    };
    //
    reha.widgets.MenuPanel.prototype.setContent = function (node) {
        this.content.html(node);
    };
    //
    reha.widgets.MenuPanel.prototype.getAnchor = function () {
        return this.anchor;
    };
    //
    reha.widgets.MenuPanel.prototype.startLoading = function () {
        this.loader.css("display", "block");
    };
    //
    reha.widgets.MenuPanel.prototype.stopLoading = function () {
        this.loader.css("display", "none");
    };

    /**
    * the menu!
    */
    reha.widgets.Menu = function (/*String*/ _menuService, /*Boolean*/ _useProxy, /*String*/ _proxy) {
        this.serviceUrl = _menuService;
        this.useProxy = _useProxy;
        this.proxy = _proxy;
    };
    // returns a deffered. this is where we get the overview of the services.
    reha.widgets.Menu.prototype.request = function (/*String*/ folder) {
        return $.getJSON(((this.useProxy) ? this.proxy + "?" : "") + this.serviceUrl + "?" + $.param({
            dir: reha.config.APPLICATION_NAME,
            f: "json"
        }));
    }
    // 
    reha.widgets.Menu.prototype.render = function (/*Object*/ arr, /*DomNode*/ anchor, /*Function*/ callback) {
        var _menu = $("<ul></ul>", {
            "class": "data-menu"
        }).appendTo(anchor);
        $.each(arr, function () {
            // build a menu header for each collection of REHA services
            var _header = $("<li></li>")
                .html($("<a>", {
                    "href": "#",
                    "html": this.name
                }))
                .addClass("menu-header")
                .appendTo(_menu);
            // if we can find more than one service associated with the collection, add those.
            if (this.items.length > 0) {
                // create another sub ul to hold the service list
                var subHeader = $("<ul></ul>")
                    .addClass("sub-menu")
                    .appendTo(_header);
                // iterate and fill. note the click handler - we need to have the "add to map" stuff here!
                $.each(this.items, function (idx, obj) {
                    var element = $("<li></li>")
                        .html("<a href=\"#\"><span>" + this.title + "</span></a>")
                        .appendTo(subHeader)
                        .css({
                            "white-space": "nowrap"
                        })
                        .click(function () {
                            callback(obj); // function passed from ui module to maintain clean(er) scope
                        });
                });
            }
        });
        // creates a jQuery UI menu from the internal menu <ul>
        return _menu.menu();
    }
    /**
    * download confirm or cancel buttons
    */
    reha.widgets.DownloadButtons = function (/*SVG*/ svg, /*esri.Graphic*/ graphic, /*Function*/ downloadCallback, /*Function*/ cancelCallback) {
       var width = 18; //ugh
       // the "click here to download button" (checkmark!)
       this.downloadButton = $("<span>")
            .addClass("svgBtn check ui-icon-check ui-icon")
            .css({
                "position": "absolute",
                "top": parseInt($(svg).attr("y")) + "px",
                "left": (parseInt($(svg).attr("x")) + (parseInt($(svg).attr("width"))) - (width * 2)) + "px"
            })
            .click(function () {
                downloadCallback();
            })
            
        this.cancelButton = $("<span>")
           .addClass("svgBtn close ui-icon-check ui-icon")
           .css({
               "position": "absolute",
               "top": parseInt($(svg).attr("y")) + "px",
               "left": ((parseInt($(svg).attr("x"))) + (parseInt($(svg).attr("width"))) - width) + "px"
           })
           .click(function () {
               cancelCallback();
           })
    };
    //
    reha.widgets.DownloadButtons.prototype.place = function(/*DomNode*/ mapContainer) {
        this.downloadButton.appendTo(mapContainer);
        this.cancelButton.appendTo(mapContainer);
    };
})(jQuery);